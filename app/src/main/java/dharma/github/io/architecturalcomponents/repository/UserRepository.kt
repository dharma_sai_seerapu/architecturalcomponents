package dharma.github.io.architecturalcomponents.repository

import androidx.lifecycle.LiveData
import dharma.github.io.architecturalcomponents.AppExecutors
import dharma.github.io.architecturalcomponents.api.GithubService
import dharma.github.io.architecturalcomponents.db.UserDao
import dharma.github.io.architecturalcomponents.model.Resource
import dharma.github.io.architecturalcomponents.model.User
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserRepository @Inject constructor(
        private val appExecutors: AppExecutors,
        private val userDao: UserDao,
        private val githubService: GithubService
) {

    fun loadUser(login: String): LiveData<Resource<User>> {
        return object : NetworkBoundResource<User, User>(appExecutors) {
            override fun saveCallResult(item: User) {
                userDao.insert(item)
            }

            override fun shouldFetch(data: User?) = data == null

            override fun loadFromDb() = userDao.findByLogin(login)

            override fun createCall() = githubService.getUser(login)
        }.asLiveData()
    }
}