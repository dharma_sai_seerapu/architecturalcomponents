package dharma.github.io.architecturalcomponents.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dharma.github.io.architecturalcomponents.ui.repo.RepoFragment
import dharma.github.io.architecturalcomponents.ui.search.SearchFragment
import dharma.github.io.architecturalcomponents.ui.user.UserFragment

@Suppress("unused")
@Module
abstract class FragmentBuildersModule {

    @ContributesAndroidInjector
    abstract fun contributeRepoFragment(): RepoFragment

    @ContributesAndroidInjector
    abstract fun contributeUserFragment(): UserFragment

    @ContributesAndroidInjector
    abstract fun contributeSearchFragment(): SearchFragment
}