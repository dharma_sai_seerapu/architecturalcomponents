package dharma.github.io.architecturalcomponents.ui.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import dharma.github.io.architecturalcomponents.AppExecutors
import dharma.github.io.architecturalcomponents.R
import dharma.github.io.architecturalcomponents.binding.FragmentDataBindingComponent
import dharma.github.io.architecturalcomponents.databinding.UserFragmentBinding
import dharma.github.io.architecturalcomponents.di.Injectable
import dharma.github.io.architecturalcomponents.ui.common.RepoListAdapter
import dharma.github.io.architecturalcomponents.ui.common.RetryCallback
import dharma.github.io.architecturalcomponents.util.autoCleared
import javax.inject.Inject

class UserFragment : Fragment(), Injectable {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject
    lateinit var appExecutors: AppExecutors

    var binding by autoCleared<UserFragmentBinding>()
    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)

    private lateinit var userViewModel: UserViewModel
    private var adapter by autoCleared<RepoListAdapter>()

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val dataBinding = DataBindingUtil.inflate<UserFragmentBinding>(
                inflater,
                R.layout.user_fragment,
                container,
                false,
                dataBindingComponent
        )
        dataBinding.retryCallback = object : RetryCallback {
            override fun retry() {
                userViewModel.retry()
            }
        }
        binding = dataBinding
        return dataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        userViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(UserViewModel::class.java)
        val params = UserFragmentArgs.fromBundle(arguments)
        userViewModel.setLogin(params.login)

        userViewModel.user.observe(this, Observer { userResource ->
            binding.user = userResource?.data
            binding.userResource = userResource
        })
        val rvAdapter = RepoListAdapter(
                dataBindingComponent = dataBindingComponent,
                appExecutors = appExecutors,
                showFullName = false
        ) { repo ->
            navController().navigate(UserFragmentDirections.showRepo(repo.owner.login, repo.name))
        }
        binding.repoList.adapter = rvAdapter
        this.adapter = rvAdapter
        initRepoList()
    }

    private fun initRepoList() {
        userViewModel.repositories.observe(this, Observer { repos ->
            adapter.submitList(repos?.data)
        })
    }

    /**
     * Created to be able to override in tests
     */
    fun navController() = findNavController()
}