package dharma.github.io.architecturalcomponents.api

import com.google.gson.annotations.SerializedName
import dharma.github.io.architecturalcomponents.model.Repo

data class RepoSearchResponse(
        @SerializedName("total_count")
        val total: Int = 0,
        @SerializedName("items")
        val items: List<Repo>
) {
    var nextPage: Int? = null
}