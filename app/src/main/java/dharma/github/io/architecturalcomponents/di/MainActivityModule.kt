package dharma.github.io.architecturalcomponents.di

import dagger.Module
import dagger.android.ContributesAndroidInjector
import dharma.github.io.architecturalcomponents.MainActivity

@Suppress("unused")
@Module
abstract class MainActivityModule {

    @ContributesAndroidInjector(modules = [FragmentBuildersModule::class])
    abstract fun contributeMainActivity(): MainActivity
}