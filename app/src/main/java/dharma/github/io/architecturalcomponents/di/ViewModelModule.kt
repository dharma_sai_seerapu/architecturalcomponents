package dharma.github.io.architecturalcomponents.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import dharma.github.io.architecturalcomponents.ui.repo.RepoViewModel
import dharma.github.io.architecturalcomponents.ui.search.SearchViewModel
import dharma.github.io.architecturalcomponents.ui.user.UserViewModel
import dharma.github.io.architecturalcomponents.viewmodel.GithubViewModelFactory

@Suppress("unused")
@Module
abstract class  ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(UserViewModel::class)
    abstract fun bindUserViewModel(userViewModel: UserViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(SearchViewModel::class)
    abstract fun bindSearchViewModel(searchViewModel: SearchViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(RepoViewModel::class)
    abstract fun bindRepoViewModel(repoViewModel: RepoViewModel): ViewModel

    @Binds
    abstract fun bindViewModelFactory(factory: GithubViewModelFactory): ViewModelProvider.Factory
}