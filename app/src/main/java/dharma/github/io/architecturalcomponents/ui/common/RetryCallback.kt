package dharma.github.io.architecturalcomponents.ui.common

interface RetryCallback {
    fun retry()
}