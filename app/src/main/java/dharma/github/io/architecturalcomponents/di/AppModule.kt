package dharma.github.io.architecturalcomponents.di

import android.app.Application
import androidx.room.Room
import dagger.Module
import dagger.Provides
import dharma.github.io.architecturalcomponents.api.GithubService
import dharma.github.io.architecturalcomponents.db.GithubDB
import dharma.github.io.architecturalcomponents.db.RepoDao
import dharma.github.io.architecturalcomponents.db.UserDao
import dharma.github.io.architecturalcomponents.util.LiveDataCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module(includes = [ViewModelModule::class])
class AppModule {

    @Singleton
    @Provides
    fun provideGithubService(): GithubService {
        return Retrofit.Builder()
                .baseUrl("https://api.github.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(LiveDataCallAdapterFactory())
                .build()
                .create(GithubService::class.java)
    }

    @Singleton
    @Provides
    fun provideDb(app: Application): GithubDB {
        return Room
                .databaseBuilder(app, GithubDB::class.java, "github.db")
                .fallbackToDestructiveMigration()
                .build()
    }

    @Singleton
    @Provides
    fun provideUserDao(db: GithubDB): UserDao {
        return db.userDao()
    }

    @Singleton
    @Provides
    fun provideRepoDao(db: GithubDB): RepoDao {
        return db.repoDao()
    }
}