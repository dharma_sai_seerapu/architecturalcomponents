package dharma.github.io.architecturalcomponents.model

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}