package dharma.github.io.architecturalcomponents.ui.repo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingComponent
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import dharma.github.io.architecturalcomponents.R
import dharma.github.io.architecturalcomponents.binding.FragmentDataBindingComponent
import dharma.github.io.architecturalcomponents.di.Injectable
import dharma.github.io.architecturalcomponents.ui.common.RetryCallback
import javax.inject.Inject
import dharma.github.io.architecturalcomponents.AppExecutors
import dharma.github.io.architecturalcomponents.databinding.RepoFragmentBinding
import dharma.github.io.architecturalcomponents.util.autoCleared

class RepoFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var repoViewModel: RepoViewModel

    @Inject
    lateinit var appExecutors: AppExecutors

    // mutable for testing
    var dataBindingComponent: DataBindingComponent = FragmentDataBindingComponent(this)
    var binding by autoCleared<RepoFragmentBinding>()

    private var adapter by autoCleared<ContributorAdapter>()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        repoViewModel = ViewModelProviders.of(this, viewModelFactory)
                .get(RepoViewModel::class.java)
        val params = RepoFragmentArgs.fromBundle(arguments)
        repoViewModel.setId(params.owner, params.name)

        val repo = repoViewModel.repo
        repo.observe(this, Observer { resource ->
            binding.repo = resource?.data
            binding.repoResource = resource
        })

        val adapter = ContributorAdapter(dataBindingComponent, appExecutors) { contributor ->
            navController().navigate(
                    RepoFragmentDirections.showUser(contributor.login)
            )
        }


        this.adapter = adapter
        binding.contributorList.adapter = adapter
        initContributorList(repoViewModel)
    }

    private fun initContributorList(viewModel: RepoViewModel) {
        viewModel.contributors.observe(this, Observer { listResource ->
            // we don't need any null checks here for the adapter since LiveData guarantees that
            // it won't call us if fragment is stopped or not started.
            if (listResource?.data != null) {
                adapter.submitList(listResource.data)
            } else {
                adapter.submitList(emptyList())
            }
        })
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val dataBinding = DataBindingUtil.inflate<RepoFragmentBinding>(
                inflater,
                R.layout.repo_fragment,
                container,
                false
        )
        dataBinding.retryCallback = object : RetryCallback {
            override fun retry() {
                repoViewModel.retry()
            }
        }
        binding = dataBinding
        return dataBinding.root
    }

    /**
     * Created to be able to override in tests
     */
    fun navController() = findNavController()
}