package dharma.github.io.architecturalcomponents.db

import androidx.room.Database
import androidx.room.RoomDatabase
import dharma.github.io.architecturalcomponents.model.Contributor
import dharma.github.io.architecturalcomponents.model.Repo
import dharma.github.io.architecturalcomponents.model.RepoSearchResult
import dharma.github.io.architecturalcomponents.model.User

@Database(
        entities = [
            User::class,
            Repo::class,
            Contributor::class,
            RepoSearchResult::class],
        version = 1,
        exportSchema = false
)
abstract class GithubDB : RoomDatabase() {

    abstract fun userDao(): UserDao

    abstract fun repoDao(): RepoDao
}